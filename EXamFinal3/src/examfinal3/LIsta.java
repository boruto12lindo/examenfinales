/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examfinal3;

/**
 *
 * @author HP I5
 */
public class LIsta<G> {
    private nodo<G> primero;
    private nodo<G> Ultimo;
    private int size;

    public LIsta() {
        this.primero = null;
        this.Ultimo = null;
        this.size = 0;
    }

    public boolean Vacia() {
        return size == 0;
    }

    public int sizeList() {
        return size;
    }

    private nodo<G> ObNodo(int lugar) {
        if (Vacia() || (lugar < 0 || lugar >= sizeList())) {
            return null;
        } else if (lugar == 0) {
            return primero;
        } else if (lugar == sizeList() - 1) {
            return Ultimo;
        } else {
            nodo<G> search = primero;
            int count = 0;
            while (count < lugar) {
                count++;
                search = search.getNext();
            }
            return search;
        }
    }

    public G get(int lugar) {
        if (Vacia() || (lugar < 0 || lugar >= sizeList())) {
            return null;
        } else if (lugar == 0) {
            return primero.getElement();
        } else if (lugar == sizeList() - 1) {
            return Ultimo.getElement();
        } else {
            nodo<G> search = ObNodo(lugar);
            return search.getElement();
        }
    }

    public G getFirst() {
        if (Vacia()) {
            return null;
        } else {
            return primero.getElement();
        }
    }

    public G getLast() {
        if (Vacia()) {
            return null;
        } else {
            return Ultimo.getElement();
        }
    }

    public G AlPrincipio(G element) {
        nodo<G> nuevelement;
        if (Vacia()) {
            nuevelement = new nodo<>(element, null);
            primero = nuevelement;
            Ultimo = nuevelement;
        } else {
            nuevelement = new nodo<G>(element,primero);
            primero = nuevelement;
        }
        size++;
        return primero.getElement();
    }

    public G alfinal(G element) {
        nodo<G> newelement;
        if (Vacia()) {
            return AlPrincipio(element);
        } else {
            newelement = new nodo<G>(element,null);
            Ultimo.setNext(newelement);
            Ultimo = newelement;
        }
        size++;
        return Ultimo.getElement();
    }

    public G añadir(G element, int index) {
        if (index == 0) {
            return AlPrincipio(element);
        } else if(index == sizeList()){
            return alfinal(element);
        }else if((index<0 || index >= sizeList())){
            return null;
        }else{
            nodo<G> nodo_prev = ObNodo(index - 1);
            nodo<G> nodo_current = ObNodo(index);
            nodo<G> newelement = new nodo<>(element,nodo_current);
            nodo_prev.setNext(newelement);
            size++;
            return ObNodo(index).getElement();
        }
    }
    
    public String listContent(){
        String str = "";
        if(Vacia()){
            str = "LISTA VACIA";
        }else{
           nodo<G> out = primero;
           while(out != null){
               str += out.getElement()+" - ";
               out = out.getNext();
           }
        }
        return str;
    }
    
    public G removerPrim(){
        if(Vacia()){
            return null;
        }else{
            G element = primero.getElement();
            nodo<G> aux = primero.getNext();
            primero = aux;
            if(sizeList() == 1){
                Ultimo = null;
            }
            size--;
            return element;
        }
    }
    
    public G removerFinal(){
        if(Vacia()){
            return null;
        }else{
            G element = Ultimo.getElement();
            nodo<G> newLast = ObNodo(sizeList()-2);
            if(newLast == null){
                Ultimo = null;
                if(sizeList() == 2){
                    Ultimo = primero;
                }else{
                    primero = null;
                }
            }else{
                Ultimo = newLast;
                Ultimo.setNext(null);
            }
            size--;
            return element;
        }
    }
    
    public G remover(int index){
        if (index == 0) {
            return removerPrim();
        } else if(index == sizeList()){
            return removerFinal();
        }else if(Vacia() || (index<0 || index >= sizeList())){
            return null;
        }else{
            nodo<G> nodo_prev = ObNodo(index-1);
            nodo<G> nodo_current = ObNodo(index);
            nodo<G> nodo_current_next = nodo_current.getNext();
            G element = nodo_current.getElement();
            nodo_current = null;
            nodo_prev.setNext(nodo_current_next);
            size--;
            return element;
        }
    }
    public int indexOf(G element){
        if(Vacia()){
            return -1;
        }else{
            nodo<G> aux = primero; 
            int index = 0;
            while(aux != null){
                if(element == aux.getElement()){
                    return index;
                }
                index++;
                aux = aux.getNext();
            }
        }
        return -1;
    }
    public G modificar(G newValue, int index){
        if(Vacia() || (index<0 || index >= sizeList())){
            return null;
        }else{
            nodo<G> aux = ObNodo(index);
            aux.setElement(newValue);
            return aux.getElement();
        }
    }    

}

