/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examfinal3;

/**
 *
 * @author HP I5
 */
public class nodo <G> {
    private G elem;
    private nodo<G> Sig;
    
    public nodo(G elem, nodo<G> Sig){
        this.elem = elem;
        this.Sig = Sig;
    }

    public G getElement() {
        return elem;
    }

    public void setElement(G elemento) {
        this.elem = elemento;
    }

    public nodo<G> getNext() {
        return Sig;
    }

    public void setNext(nodo<G> Sig) {
        this.Sig = Sig;
    }
}
