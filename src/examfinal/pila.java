package examfinal;

/**
 * @author Melina Karen Ticra Agular CI: 15208024
 */
public class pila {

    char arr[];
    int cima;

    pila() {
        this.arr = new char[5];
        this.cima = -1;
    }

    public boolean vacio() {
        return (cima == -1);
    }

    public void push(char elemento) {
        cima++;
        if (cima < arr.length) {
            arr[cima] = elemento;
        } else {
            char aux[] = new char[arr.length + 5];
            for (int i = 0; i < arr.length; i++) {
                aux[i] = arr[i];
            }
            arr = aux;
            arr[cima]=elemento;
        }
    }
    public char peek(){
        return arr[cima];
    }
    public char pop(){
        if(!vacio()){
            int auxTop=cima;
            cima--;
            char returnCima=arr[auxTop];
            return returnCima;
        }else{
            return '-';
        }
    }
}
