package examfinal;
/**
 * @author Melina Karen Ticra Agular
 * CI: 15208024
 */
public class ExamFinal {
    public static void main(String[] args) {
        String pil ="aabaa";
        pila pi=new pila();
        for(int i=0;i<pil.length();i++){
            pi.push(pil.charAt(i));
        }
        String revesep="";
        while(!pi.vacio()){
            revesep=revesep+pi.pop();
        }
        if(pil.equals(revesep)){
            System.out.println("Es palindroma");
        }else{
            System.out.println("No es palindroma");
        }
    }
    
}
